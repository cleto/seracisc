# -*- coding:utf-8 -*-

# Tipos de memoria
# S  -> entrelazada simple
# C  -> entrelazada compleja
# CS -> caché separada
# CU -> caché unificada
# Por defecto es CS
#
# Tipos de detección
# E -> estática
# D -> dinámica
# Por defecto es E
#
# Cortocircuito
# y/n
# Por defecto 'y'
#
# Salto retardado
# y/n
# Por defecto 'n'


[Ejercicio1]
forwarding = y
memory = CU
detection = D
delayed_skip = n
code =
     R[1] <- ADD(R[2], MEM[3])
     R[1] <- ADD(R[2], MEM[3])

[Ejercicio2]
forwarding = y
memory = S
detection = E
delayed_skip = y
code =
     R[1] <- ADD(R[2], MEM[3])
     R[1] <- ADD(R[2], MEM[3])
     MEM[1] <- ADD(MEM[2], MEM[3])
