#!/usr/bin/python
# -*- coding:utf-8; -*-

import ConfigParser
import sys
import os

default = {"forwarding":"y",
           "memory":"CS",
           "detection":"D"}

class MemoryType:
    SCACHE, UCACHE, SIMPLE, COMPLEX  = range(4)

memory_types = {"S": MemoryType.SIMPLE,
                "C": MemoryType.COMPLEX,
                "CS": MemoryType.UCACHE,
                "CU": MemoryType.SCACHE }

class Exercise:
    def __init__(self, name, forw, detect, mem, skip, code):
        self.name = name

        self.forwarding = forw != "n"

        self.delayed_skip = skip != "n"

        self.memory = memory_types['CS']
        try:
            self.memory = memory_types[mem]
        except KeyError,e:
            print "Memory type '%s' not recognized. Using 'CS' by default" % mem


        self.dynamic_detect = detect == "D"

        self.code = code

class PDFBuilder:
    class Exercise(Exercise):
        def __init__(self, name, forw, detect, mem, skip, code):
            Exercise.__init__(self, name, forw, detect, mem, skip, code)
            self.output = None

        def build(self, output):
            self.output = output
            name = """\\begin{center}
\\Large{%s}
\\end{center}
""" % self.name

            self.output.write(name)
            self.__wording()
            self.__tables()

        def __wording(self):
            symbol = {False:"square",
                      True:"blacksquare"}

            mflags = [symbol[False]] * 4
            mflags[self.memory] = symbol[True]

            t = tuple(mflags) + (symbol[self.dynamic_detect],
                 symbol[not self.dynamic_detect],
                 symbol[self.delayed_skip],
                 symbol[self.forwarding])

            flags = """
\\begin{tabular}{l l l l}
$\\%s$ C. Unificada & $\\%s$ C. Separadas & $\\%s$ E. Simple & $\\%s$ E. Compleja\\\\
$\\%s$ D. Dinámica & $\\%s$ D. Estática & $\\%s$ S. Retardado & $\\%s$ Cortocircuito \\\\
\\end{tabular}
""" % t
            self.output.write(flags)

            code = "\n\\bigskip\n"
            code += "\\begin{enumerate}[label=\Alph*:, start=9, noitemsep]\n"
            for line in self.code.strip("\n").split("\n"):
                code += "\\item \\texttt{%s}\n" % line

            code += "\\end{enumerate}\n\\bigskip"
            self.output.write(code)

        def __tables(self):

            columns = [[chr(73 + x) for x in \
                            range(len(self.code.strip("\n").split("\n")))],
                       ["FPC", "DREA", "MPC", "ALU", "WRR", "HELP"],]

            for column in columns:
                table = "\n\\begin{minipage}{\linewidth}\n"
                table += "\\begin{tabular}{%s}\n" % ("l"+"|l"*20)
                table += "&"+"&".join([str(x) for x in range(1,21)])+"\\\\\\hline\n"
                for line in column:
                    table += line + "&"*20 + "\\\\\hline\n"

                table += "\\end{tabular}\n\\end{minipage}\n\\bigskip\\\\"
                self.output.write(table)



    def __init__(self, exercices):
        self.ex = exercices
        self.output = None

    def build(self, output):
        print "Building as PDF file to '%s'" % output
        filename = os.path.splitext(output)[0] + ".tex"
        self.output = file(filename, 'w')

        self.__header()

        for e in self.ex:
            e.build(self.output)

        self.output.write("\end{document}\n")
        self.output.close()
        os.system("pdflatex %s" % filename)

        return 0

    def __header(self):
        header = """% -*- coding:utf-8 -*-
\\documentclass[a4paper, twoside]{article}
\\usepackage[utf8]{inputenc}
\\usepackage{enumitem}
\\usepackage[left=2cm, right=2cm]{geometry}
\\usepackage{amsmath, amssymb}
\\begin{document}
"""
        self.output.write(header)


class SeraCisc:
    def __init__(self, exercises, output):
        self.builders  = {"pdf": PDFBuilder}
        self.exercises = exercises
        self.output    = output

    def build(self):
        ext = os.path.splitext(self.output)[1].strip('.')
        if not ext:
            ext = "pdf"

        if ext not in self.builders.keys():
            print "Extension '%s' not supported." % ext
            return 1

        cp = ConfigParser.ConfigParser(default)
        cp.read(self.exercises)

        builder = self.builders[ext]

        exercises = []
        for e in sorted(cp.sections()):
            try:
                # keys = ['code', 'mem']
                # a = Exercise(e, **dict([((name, cp.get(e, k)) for k in keys)]))
                code   = cp.get(e, "code")
                mem    = cp.get(e, "memory")
                detect = cp.get(e, "detection")
                forw   = cp.get(e, "forwarding")
                skip   = cp.get(e, "delayed_skip")
                exercise = builder.Exercise(e, forw, detect, mem, skip, code)
                exercises.append(exercise)
            except ConfigParser.NoOptionError, er:
                print er
                return 1

        builder(exercises).build(self.output)

        return 0

def show_help():
    print "Usage: %s <exercises> <output> " % sys.argv[0]

if (len(sys.argv) != 3):
    show_help()
    sys.exit(1)

app = SeraCisc(sys.argv[1], sys.argv[2])
sys.exit(app.build())

